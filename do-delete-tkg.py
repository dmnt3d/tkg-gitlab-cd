import subprocess
import json

# get current list of tkg cluster(s) and store in json
try:
    out = subprocess.run(['tkg', 'get', "cluster","--config" ,".tkg/config.yaml", "-o", "json"], stdout=subprocess.PIPE, check=True)
    tkgClusters = json.loads(out.stdout.decode('utf-8'))
except subprocess.CalledProcessError as e:
  print(e.output)
  exit(1)

# Check DELETED Files
result = subprocess.run(['git', 'diff', "HEAD~", "--name-only", "--diff-filter=D"], stdout=subprocess.PIPE)
lines = result.stdout.decode('utf-8').split('\n')
print (result)
for a in lines:
  if a.__contains__("clusters"):
    clusterName = a[a.rindex('/')+1:a.rindex('.')]
    # check if cluster exists in tkg
    for x in tkgClusters:
      if str(x['name']) == clusterName:    
        print ("Found cluster to DELETE")
        print ("Cluster Name: "+clusterName)
        try:
          subprocess.run(['tkg', 'delete', "cluster", clusterName, "--config" ,".tkg/config.yaml","--yes"], stdout=subprocess.PIPE, check=True)
        except subprocess.CalledProcessError as e:
          print(e.output)
          exit(1)
      