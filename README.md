# Continuous deployment Tanzu Kubernetes cluster using Gitlab CICD pipelines

## Pre-requisites
- TKG Management Cluster already configured
- Shell gitlab runner (preferrably linux)
- Pre-setup the linux to have the tkg cli/kube config pre-loaded
NOTE: this can also be achieved by using VARIABLES within the gitlab cicd pipeline
- Change the config.yaml with your .tkg config

## Directions
### To add TKG cluster
- Copy tkgcluster.json and rename to what the cluster name will be
NOTE: file should be inside the clusters folder
- Fill-out the number master and worker nodes the cluster will be using
- commit to Gitlab

![Add Cluster](https://letsdocloud.com/wp-content/uploads/2020/09/Add-Cluster-edited-3.gif)

### To modify/scale the TKG cluster
- Modify the cluster json with the desired number
- Commit to Gitlab

![Scale Cluster](https://letsdocloud.com/wp-content/uploads/2020/09/Scale-down-edited.gif)

### To delete the tkg cluster
- Delete the cluster json file
- Commit to Gitlab

![Delete Cluster](https://letsdocloud.com/wp-content/uploads/2020/09/Delete-Cluster-edited.gif)


#### How it works:
- Python scripts watches the git diff and performs the action depending on what happens in the clusters folder
- Python executes a subprocess to perform the actions.


### Things to improve:
- Extend kubernetes cluster definition options. 
Originally, I wanted to use kustomize for the cluster definition but the variable substitution would be a nighmare and it would end-up looking like helm.  

- Extend installed software to provisioned kubernetes
This can easily be added by adding switch on the cluster definition and doing kubectl apply after getting the kubeconfig of the cluster

- Optimize gitlab CICD execution to only perform on folder changes
Right now, there's no way to do this in gitlab-ci.yml so I ended up parsing the result of git dif. Numerous issues already raised and I think changes are on their way. https://gitlab.com/gitlab-org/gitlab-foss/-/issues/19813

- Cluster Testing
No clusting implemented. This is just for POC :)

#### Blog Post:
https://letsdocloud.com/?p=868

