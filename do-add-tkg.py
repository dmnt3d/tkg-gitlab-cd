import subprocess
import json

#git diff HEAD~ --name-only --diff-filter=A | grep clusters
#process = subprocess.Popen(['git', 'diff', "HEAD~", "--name-only", "--diff-filter=A", "|", "grep", "clusters"], stdout=PIPE, stderr=PIPE)

# Query if there are any Additions
result = subprocess.run(['git', 'diff', "HEAD~", "--name-only", "--diff-filter=A"], stdout=subprocess.PIPE)
lines = result.stdout.decode('utf-8').split('\n')
for a in lines:
  if a.__contains__("clusters"):
    print ("Found new clusters to create: "+ a)
    with open(a) as f:
      data = json.load(f)
    clusterName = a[a.rindex('/')+1:a.rindex('.')]
    print ("Cluster Name: "+clusterName)
    print ("Master Node: "+ str(data['master']))
    print ("Worker Node: "+ str(data['worker']))
    
    
    try:
      subprocess.run(['tkg', 'create', "cluster", clusterName, "--config" ,".tkg/config.yaml", "--size", "medium","--controlplane-machine-count="+str(data['master']), "--worker-machine-count="+ str(data['worker']),"--plan", "dev"], stdout=subprocess.PIPE, check=True)
    except subprocess.CalledProcessError as e:
      print(e.output)
      exit(1)

