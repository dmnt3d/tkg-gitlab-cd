import subprocess
import json

# get current list of tkg cluster(s) and store in json
try:
    out = subprocess.run(['tkg', 'get', "cluster","--config" ,".tkg/config.yaml", "-o", "json"], stdout=subprocess.PIPE, check=True)
    tkgClusters = json.loads(out.stdout.decode('utf-8'))
except subprocess.CalledProcessError as e:
  print(e.output)
  exit(1)

# Check Modified Files
result = subprocess.run(['git', 'diff', "HEAD~", "--name-only", "--diff-filter=M"], stdout=subprocess.PIPE)
lines = result.stdout.decode('utf-8').split('\n')
for a in lines:
  if a.__contains__("clusters"):
    with open(a) as f:
      data = json.load(f)
    clusterName = a[a.rindex('/')+1:a.rindex('.')]
    # check if cluster exists in tkg
    for x in tkgClusters:
      if str(x['name']) == clusterName:    
        print ("Found cluster to MODIFY:")
        print ("Cluster Name: "+clusterName)
        print ("Master Node FROM " + x['controlplane'] + " TO " + str(data['master']))
        print ("Worker Node FROM " + x['workers'] + " TO " + str(data['worker']))
        try:
          subprocess.run(['tkg', 'scale', "cluster", clusterName, "--config" ,".tkg/config.yaml", "--controlplane-machine-count="+str(data['master']), "--worker-machine-count="+ str(data['worker'])], stdout=subprocess.PIPE, check=True)
        except subprocess.CalledProcessError as e:
          print(e.output)
          exit(1)
      